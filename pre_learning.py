# -*- coding: utf-8 -*-

import nltk
import os,errno
from nltk.tag import brill
from nltk.corpus import indian
import json

dic={}

def splitArray(arr): # splite the data tranning data set into word VS tag
    try:
		return map(lambda token: (token.split('\\')[0],token.split('\\')[1].split(".")[0]), arr)
    except:
		pass

# create lexicon for primary knowledge
def wordLexicon(array):
    words={}
    tags={}
    sentence={}
    max=0
   
    for i in array:
        a=''
        b=[]
        x='o'
        tem_sen={}
        
        for j in i:
            y=y+1
            if j[0] in words:
                if j[1] in words[j[0]]:
                    words[j[0]][j[1]]=words[j[0]][j[1]]+1
                else:
                    words[j[0]].update({j[1]:1})
            else:
                words.update({j[0]:{j[1]:1}})
            
            if a!='':
                if j[1] in tags:
                    if a in tags[j[1]]:
                        tags[j[1]][a]=tags[j[1]][a]+1
                    else:
                        tags[j[1]].update({a:1})
                else:
                    tags.update({j[1]:{a:1}})
            a=j[1]
            b.append(j[1])
            if x=='o':
                if j[1] in sentence:
                    tem_sen=sentence[j[1]]
                    
                else :
                    sentence.update({j[1]:{}})
                    tem_sen= sentence[j[1]]
            elif j[1]==x:
                pass
            else:
                if j[1] in tem_sen:
                    tem_sen=tem_sen[j[1]]
                else :
                    tem_sen.update({j[1]:{}})
                    tem_sen= tem_sen[j[1]]
            x=j[1]
                 

    # store words vs tag as jeson format
    with open('Knowledge/words','w') as f:
        json.dump(words, f)
    with open('Knowledge/tags','w') as f:
        json.dump(tags, f)
    with open('Knowledge/sentence','w') as f:
        json.dump(sentence, f)

# create file to save number of sentence and words
def stats(arr,dataset): 
    numLines=len(arr)
    numWords=len(reduce(lambda x,y:x+y,arr))
    lines=["Stats for dataset "+dataset+"\n","Number of lines: "+str(numLines)+"\n","Number of words: "+str(numWords)+"\n"]
    fwrite=open("analyzedData/"+dataset+".stat",'w')
    fwrite.writelines(lines)
    fwrite.close()

#to calculate frequency of sepcific tag
def posFrequency(arr,dataset): 
    arr=reduce(lambda x,y:x+y,arr)
    new_arr=map(lambda a:a[1],arr)
    lines=[]
    for e in set(new_arr):
        lines.append((e,new_arr.count(e)))
    lines=sorted(lines,key=lambda a:a[1],reverse=True)
    lines=map(lambda (a,b): str(a)+";"+str(b)+"\n", lines)
    fwrite=open("analyzedData/"+dataset+".freq",'w')
    fwrite.writelines(lines)
    fwrite.close()


fread=open("nltk_data/nltr")
lines=fread.readlines()
fread.close()

filtered_sents2=[]
lines=map(lambda line:line.split(),lines)
lines=map(lambda line:splitArray(line), lines)

for i,line in enumerate(lines):  #this is not necessary for learning
   
    try:
        tagger=nltk.tag.UnigramTagger([line])
        filtered_sents2.append(line)
        #print line
    except (TypeError, ValueError):
        pass

try:
	os.makedirs("analyzedData/")
except OSError as exc:
	if exc.errno == errno.EEXIST:
		pass
	else:
		raise
wordLexicon(filtered_sents2)
#save learning data information in analyzedData folder
'''stats(filtered_sents2,"nltr")  
posFrequency(filtered_sents2,"nltr")'''
